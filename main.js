const aud = document.querySelector('audio');
const body = document.querySelector('body');
body.addEventListener('click', playAudio);

function playAudio() {
    return aud.play();
}